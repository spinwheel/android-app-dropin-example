package com.example.spinwheelexamplejava;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


public class MainActivity extends AppCompatActivity {

    Button connectButton;
    String secretKey = BuildConfig.SECRET_KEY;

    private RequestQueue mRequestQueue;
    private String url = "https://dev-swsl-api.spinwheel.io/v1/dim/token";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        connectButton = findViewById(R.id.btnConnect);
        connectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTokenAndOpenDIM();
            }
        });
    }

    private void getTokenAndOpenDIM() {

        try {
            //RequestQueue initialized
            mRequestQueue = Volley.newRequestQueue(this);

            JSONObject jsonBody = new JSONObject();

            jsonBody.put("module", "loan-servicers-login");
            jsonBody.put("extUserId", "randomuser-1234567");
            jsonBody.put("redirectUri", "file:///android_asset/index.html");
            jsonBody.put("tokenExpiryInSec", "30000");

            JsonObjectRequest jsonObject = new JsonObjectRequest(Request.Method.POST, url, jsonBody, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {

                    try {
                        JSONObject data = response.getJSONObject("data");
                        String token = data.getString("token");

                        Intent intent = new Intent(getApplicationContext(), BaseActivity.class);
                        intent.putExtra("token", token);
                        intent.putExtra("dropInToLoad", "connect");
                        startActivity(intent);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                    onBackPressed();

                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/json");
                    params.put("Authorization","Bearer " + secretKey);
                    return params;
                }
            };
            mRequestQueue.add(jsonObject);
        } catch (Exception ex) {
            Log.e("error", ex.toString());
        }
    }
}