package com.example.spinwheelexamplejava;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class BaseActivity extends AppCompatActivity {
    String token;
    String dropInToLoad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_second);

        Intent intent= getIntent();
        Bundle bundle = intent.getExtras();

        if(bundle!=null)
        {
            token = (String) bundle.get("token");
            dropInToLoad = (String) bundle.get("dropInToLoad");
            Intent intent2 = new Intent(getApplicationContext(), SecondActivity.class);
            intent2.putExtra("dropInToLoad", dropInToLoad);
            intent2.putExtra("token", token);
            startActivity(intent2);
        }
    }
}