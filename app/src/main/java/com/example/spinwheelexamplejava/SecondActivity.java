package com.example.spinwheelexamplejava;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {

    private WebView webview;
    String token;
    String dropInToLoad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent intent= getIntent();
        Bundle bundle = intent.getExtras();

        if(bundle!=null)
        {
            token = (String) bundle.get("token");
            dropInToLoad = (String) bundle.get("dropInToLoad");
        }

        webview = findViewById(R.id.cdnContainer);
        webview.setWebViewClient(new CustomWebViewClient());
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setAllowFileAccessFromFileURLs(true);
        webview.getSettings().setAllowUniversalAccessFromFileURLs(true);
        webview.loadUrl("file:///android_asset/index.html");
        webview.addJavascriptInterface(new JavaScriptInterface(this), "Android");
    }

    private class CustomWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            loadJs(view);
        }
    }

    private void loadJs( WebView webView) {
        String js = "javascript:(function f() {\n" +
                "          let loaded = setInterval(()=>{\n" +
                "            if(SpinwheelHandler) {\n" +
                "              SpinwheelHandler.init({token:'" +  token + "' , dropInToLoad:  '" + dropInToLoad + "'});\n" +
                "              SpinwheelHandler.loadSpinWheel();\n" +
                "              clearInterval(loaded);\n" +
                "            }\n" +
                "          }, 500);\n" +
                "        })()";
        webView.loadUrl(js);
    }

    public class JavaScriptInterface {
        Context mContext;

        JavaScriptInterface(Context c) {
            mContext = c;
        }

        @JavascriptInterface
        public void handleOnSuccess(String toast) {
            Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(), BaseActivity.class);
            intent.putExtra("dropInToLoad", "loanpal");
            intent.putExtra("token", token);
            startActivity(intent);
            finish();
        }

        @JavascriptInterface
        public void handleOnLoad() {

        }

        @JavascriptInterface
        public void handleOnExit() {
            finish();
        }
    }
}