# About Spinwheel Android App Dropin Example

This repository is designed to showcase a minimalistic implementation of Spinwheel's Drop-In Modules. The app models how a third-party client could quickly be up and running using the Spinwheel functions: connecting a loan using the Connect DIM, displaying a customer's loan details with the Loan Pal DIM, and Navigating across different DIMs.

NOTE: This application was not originally intended to be a model, so it takes a number of liberties that are not in line with best practices like: not properly storing credentials in environmental variables, not using a proper router/controller/services pattern, not logging anything, and not making even the slightest attempt at handle errors gracefully. **As such, DO NOT, under any circumstance, directly cut and paste the code from this repository.**

# Getting Started
## Clone

To begin, open a terminal window and clone this repository onto your local machine using the command:

```
git clone git@bitbucket.org:spinwheel/android-app-dropin-example.git
```

Once cloned, follow below to get started:


### This App is build using Android Studio (Link: https://developer.android.com/studio#downloads) with openjdk 11.0.11

*Please make sure to have met above requirements before running below*


### Instructions to run the app locally

1. Open the project in android studio
2. Create a apikey.properties file in root directory and add secretKey in that file as 

	SECRET_KEY="your-key"

*Note:* Although apikey.properties is added to .gitignore, do Not try to add it to remote repo


Now we can test the code in any emulator connected to Android Studio by running the build.